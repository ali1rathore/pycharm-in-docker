#!/bin/bash

mkdir -p ~/.PyCharm ~/.PyCharm.java

docker run --rm \
	-d \
	--env DISPLAY=${DISPLAY} \
	--volume /tmp/.X11-unix:/tmp/.X11-unix \
	--volume ~/.PyCharm:/home/developer/.PyCharm \
	--volume ~/.PyCharm.java:/home/developer/.java \
	--volume /code:/code \
	--user 1000:1000 \
	--name pycharm \
	registry.gitlab.com/ali1rathore/pycharm-in-docker/pycharm:latest $@
