FROM python:3.6-jessie

LABEL maintainer "Ali R <incoming+ali1rathore-pycharm-in-docker-10302179-issue-@incoming.gitlab.com>"

ARG pycharm_source=https://download.jetbrains.com/python/pycharm-community-2018.3.3.tar.gz
ARG pycharm_local_dir=.PyCharmCE2018.3

RUN apt-get update \
	&& apt-get install -y libxtst6 \
	&& rm -rf /var/lib/apt/lists/* \
	&& useradd -ms /bin/bash developer



WORKDIR /opt/pycharm

RUN curl -fSL $pycharm_source -o /opt/pycharm/installer.tgz \
  && tar --strip-components=1 -xzf installer.tgz \
  && rm installer.tgz \
  && /usr/local/bin/python3 /opt/pycharm/helpers/pydev/setup_cython.py build_ext --inplace

USER developer
ENV HOME /home/developer

RUN mkdir /home/developer/.PyCharm \
  && ln -sf /home/developer/.PyCharm /home/developer/$pycharm_local_dir

CMD [ "/opt/pycharm/bin/pycharm.sh" ]
