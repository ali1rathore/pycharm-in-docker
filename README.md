Docker container to run PyCharm Community Edition (https://www.jetbrains.com/pycharm/)

### Usage

The following command will fetch the `pycharm.sh` script to `~/bin`

```bash
mkdir -p ~/bin && curl -o ~/bin/pycharm.sh https://gitlab.com/ali1rathore/pycharm-in-docker/raw/master/pycharm.sh && chmod a+x ~/bin/pycharm.sh
```

see `pycharm.sh` for more info.

### Notes

The IDE will have access to Python 3.6 both and to Git as well.
Project folders need to be mounted like `-v ~/Project:/home/developer/Project`.
The actual name can be anything - I used something random to be able to start multiple instances if needed.

To use `pip` (or `pip3`) either use the terminal in PyCharm or install from the terminal from inside the container like `docker exec -it pycharm-running bash` then install using **pip**.
